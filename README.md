Challenge
------------------------
Utilizing jQuery to do an AJAX call, produce javascript and PHP code that will log a client into the system and start a session for them.  

Database
-------------------------
The client database table has the fields: 
(int)client_id,(varchar)user_name,(varchar)password_hash,(datetime)last_login.

Result
--------------------------
Very simple login

Setup
--------------------------
create db 'test' and import using members.sql file provided
edit login.php to include your db credentials

Use the user "test" with password "user" (without quotes)

Once logged in, refresh the page to get a greeting.
