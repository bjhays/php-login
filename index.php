<?php
$message = "";
$sid="";
if(isset($_COOKIE['PHPSESSID'])){$sid=$_COOKIE['PHPSESSID'];}
if (defined(SID)){$sid=SID;}

if($sid!=""){ session_start(); }
if(isset($_SESSION['userid'])){
	echo '<pre>'.print_r($_SESSION,true).'</pre>';
	$message = "Hi ".$_SESSION['username']."!";
}
?>
<html>
<head>
	<title>PHP Login Script</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>
	jQuery(function(){
		$("#loginForm").submit(function(event) {
		  	event.preventDefault();
		  	var form = $( this ),
			    url = form.attr("action");
		 
			var posting = $.post( url, form.serialize());
			posting.done(function( data ) {
			    var json = jQuery.parseJSON(data);
			    $("#loginStatus").empty().html( json.message );
			  });
		});

		$("#loginOutForm").submit(function(event) {
		  	event.preventDefault();
		  	var form = $( this ),
			    url = form.attr("action");
		 
			var posting = $.post( url, form.serialize());
			posting.done(function( data ) {
			    var json = jQuery.parseJSON(data);
			    $("#loginStatus").empty().html( json.message );
			  });
			location.reload();
		});
	});

	</script>
</head>
<body>
	<?php if($message==""){ ?>
	<h1 class="pagetitle">Please enter your login information:</h1>
	<form id="loginForm" action="login.php" method="post">
		<b>User: </b><input type="text" name="myusername" />
		<b>Pass: </b><input type="password" name="mypassword" />
		<br/>
		<input type="submit" name="submitButton" value="Login">
	</form>
	<?php } else {
		echo "<h1>$message</h1>";
	} ?>
	<form id="loginOutForm" action="login.php" method="post">
		<input type="hidden" name="logout" value="1" />
		<input type="submit" name="submitButton" value="Logout">
	</form>
	
	<div id="loginStatus"></div>
</body>
</html>